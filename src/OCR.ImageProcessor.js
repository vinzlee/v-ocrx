async function imageProcessor(file) {
    return new Promise(function (resolve) {
        let fileData = file.split("/");
        const filePath = fileData.splice(0, fileData.length - 1).toString().replace(/,/g, '/');
        const fileName = fileData.toString().replace(/,/g, '.');
        fileData = fileName.split(".");
        const fileExt = fileData[fileData.length - 1];
        if (fileExt.toLowerCase() === "bmp") {
            resolve(file);
        }
        else {
            if (fileExt.toLowerCase() === "jpg" || fileExt.toLowerCase() === "jpeg" || fileExt.toLowerCase() === "png") {
                const res = convertImage(file, fileName, filePath);
                resolve(res);
            }
            else {
                return "not valid file extension";
            }
        }
    })
}

function convertImage(file, fileName, filePath) {
    return new Promise(function (resolve) {
        const Jimp = require('jimp');
        Jimp.read(file).then(data => {
            const fileRes = filePath + "/" + fileName + ".bmp";
            data.writeAsync(fileRes);
            resolve("/" + fileRes);
        });
    })
}

module.exports = imageProcessor;