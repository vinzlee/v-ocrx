const ocrx = require('ocr');
const imageProcessor = require('./OCR.ImageProcessor');

function ocrxRecog(params) {
    return new Promise(function (resolve) {
        ocrx.recognize(params, function (err, doc) {
            if (err) {
                console.log(err);
                return err;
            }
            var wordArray = doc.getWords().map(function (element) {
                var obj = {};
                obj[element.text] = element.area;
                return obj;
            });
            resolve(wordArray);
        });
    })
}

async function runRecog(inputfile) {
    const image = await imageProcessor(inputfile);
    console.log(image);

    const params = {
        input: image,
        language: 'english',
        defaultDpi: [300,300]
    };
    const res = await ocrxRecog(params);
    console.log("Result", res);
}

module.exports = runRecog